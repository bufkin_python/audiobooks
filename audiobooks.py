#! /usr/bin/env/python311

from pyttsx3 import init
from pypdf import PdfReader

if __name__ == '__main__':
    with open('exercise.pdf', 'r') as book:
        full_text = ""

        reader = PdfReader(book)

        audio_reader = init()
        audio_reader.setProperty("rate", 100)

    for page in range(len(reader.pages)):
        next_page = reader.getPage(page)
        content = next_page.extractText()
        full_text += content

        audio_reader.save_to_file(full_text, "audiobook.mp3")
        audio_reader.runAndWait()
